#ifndef TEST_H
#define TEST_H


class Test
{
    public:
        enum Door {
            goat,
            car
        };
        Test();
        bool run(bool changeDecison);
        virtual ~Test();
    protected:
    private:
        Door stage[3];
        int getOtherGoat(int g);
        inline int getNextPos(int pos, int g) {
            return 3-pos-g;
        }
};

#endif // TEST_H
