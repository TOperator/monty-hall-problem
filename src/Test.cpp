#include "Test.h"
#include <cstdlib>

Test::Test()
{
    //ctor
    for (int i = 0; i < 3; ++i) {
        stage[i] = goat;
    }
    int c = rand() % 3;
    stage[c] = car;
}

int Test::getOtherGoat(int g) {
    switch (g) {
    case 0:
        if (stage[1] == goat) {
            return 1;
        } else {
                return 2;
        }
    case 1:
        if (stage[0] == goat) {
            return 0;
        } else {
            return 2;
        }
    case 2:
        if (stage[0] == goat) {
            return 0;
        } else {
            return 1;
        }
    default:
        return -1;
    }
}

bool Test::run(bool changeDecison) {
    int pos = rand() % 3;
    int g = getOtherGoat(pos);
    if (changeDecison) {
        pos = getNextPos(pos, g);
    }
    if (stage[pos] == car) {
        return true;
    } else {
        return false;
    }
}

Test::~Test()
{
    //dtor
}
