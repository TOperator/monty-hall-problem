#include <iostream>
#include <cmath>
#include "Test.h"

using namespace std;

int main()
{
    int testNo = 10000;
    Test dumb[testNo];
    Test smart[testNo];
    int dumbRight = 0;
    int smartRight = 0;
    for (int i = 0; i < testNo; ++i) {
        //dumb[i] = new Test();
        if (dumb[i].run(false)) {
            ++dumbRight;
        }
        //smart[i] = new Test();
        if (smart[i].run(true)) {
            ++smartRight;
        }
    }
    double dumpPercentage = round(((double) dumbRight / (double) testNo)*100);
    double smartPercentage = round(((double) smartRight / (double) testNo)*100);
    cout << "Result to the monty hall problem:" << endl;
    cout << "Dumb choise: " << dumpPercentage << "% (" << dumbRight << "/" << testNo << ") chance to get the car" << endl;
    cout << "Smart choise: " << smartPercentage << "% (" << smartRight << "/" << testNo << ") chance to get the car" << endl;
    return 0;
}
